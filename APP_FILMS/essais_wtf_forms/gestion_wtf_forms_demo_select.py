"""
    Fichier : gestion_adresse_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les adresse.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /adresse_delete
    
    Test : ex. cliquer sur le menu "adresse" puis cliquer sur le bouton "DELETE" d'un "adresse"
    
    Paramètres : sans
    
    But : Effacer(delete) un adresse qui a été sélectionné dans le formulaire "adresse_afficher.html"
    
    Remarque :  Dans le champ "nom_adresse_delete_wtf" du formulaire "adresse/adresse_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    adresse_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_adresse"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_adresse.data:

            if form_demo.submit_btn_ok_dplist_adresse.data:
                print("adresse sélectionné : ",
                      form_demo.adresse_dropdown_wtf.data)
                adresse_selectionne = form_demo.adresse_dropdown_wtf.data
                form_demo.adresse_dropdown_wtf.choices = session['adresse_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_adresse_afficher = """SELECT id_adresse, ville FROM t_adresse ORDER BY id_adresse ASC"""
                mc_afficher.execute(strsql_adresse_afficher)

            data_adresse = mc_afficher.fetchall()
            print("demo_select_wtf data_adresse ", data_adresse, " Type : ", type(data_adresse))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            adresse_val_list_dropdown = []
            for i in data_adresse:
                adresse_val_list_dropdown.append(i['ville'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # adresse_val_list_dropdown = [(i["id_adresse"], i["ville"]) for i in data_adresse]

            print("adresse_val_list_dropdown ", adresse_val_list_dropdown)

            form_demo.adresse_dropdown_wtf.choices = adresse_val_list_dropdown
            session['adresse_val_list_dropdown'] = adresse_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.adresse_dropdown_wtf.data = "philosophique"
            adresse_selectionne = form_demo.adresse_dropdown_wtf.data
            print("adresse choisi dans la liste :", adresse_selectionne)
            session['adresse_selectionne_get'] = adresse_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           adresse_selectionne=adresse_selectionne)
