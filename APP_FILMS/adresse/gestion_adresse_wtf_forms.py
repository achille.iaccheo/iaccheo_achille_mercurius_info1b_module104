"""
    Fichier : gestion_adresse_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouteradresse(FlaskForm):
    """
        Dans le formulaire "adresse_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_adresse_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_adresse_wtf = StringField("Clavioter le adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_adresse_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer adresse")


class FormWTFUpdateadresse(FlaskForm):
    """
        Dans le formulaire "adresse_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_adresse_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_adresse_update_wtf = StringField("Clavioter le adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_adresse_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Update adresse")


class FormWTFDeleteadresse(FlaskForm):
    """
        Dans le formulaire "adresse_delete_wtf.html"

        nom_adresse_delete_wtf : Champ qui reçoit la valeur du adresse, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "adresse".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_adresse".
    """
    nom_adresse_delete_wtf = StringField("Effacer ce adresse")
    submit_btn_del = SubmitField("Effacer adresse")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
