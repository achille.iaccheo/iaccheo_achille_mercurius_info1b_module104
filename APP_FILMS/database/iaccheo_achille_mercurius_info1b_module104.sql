-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 13 Juin 2021 à 21:09
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `iaccheo_achille_mercurius_info1b_module104`
--
CREATE DATABASE IF NOT EXISTS `iaccheo_achille_mercurius_info1b_module104` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `iaccheo_achille_mercurius_info1b_module104`;

-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE `t_adresse` (
  `id_adresse` int(11) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `Rue` varchar(255) NOT NULL,
  `code_postal` int(11) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `departement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `t_adresse`
--

INSERT INTO `t_adresse` (`id_adresse`, `ville`, `Rue`, `code_postal`, `pays`, `departement`) VALUES
(4, 'wefqwef', '', 0, '', ''),
(5, 'awefqwef', '', 0, '', ''),
(6, 'dfsaffgf', '', 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_article`
--

CREATE TABLE `t_article` (
  `id_article` int(11) NOT NULL,
  `nom_article` varchar(42) NOT NULL,
  `type_article` varchar(42) NOT NULL,
  `stock` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `taille` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_commander`
--

CREATE TABLE `t_commander` (
  `id_commander` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_article_d` int(11) NOT NULL,
  `date_commande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_genre`
--

CREATE TABLE `t_genre` (
  `id_genre` int(11) NOT NULL,
  `genre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `t_habiter`
--

CREATE TABLE `t_habiter` (
  `id_habiter` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `fk_adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `t_habiter`
--

INSERT INTO `t_habiter` (`id_habiter`, `fk_user`, `fk_adresse`) VALUES
(7, 4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `t_payer`
--

CREATE TABLE `t_payer` (
  `id_payer` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_article_id` int(11) NOT NULL,
  `date_paiement` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `prenom_user` varchar(255) NOT NULL,
  `nom_user` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `fk_genre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `t_user`
--

INSERT INTO `t_user` (`id_user`, `prenom_user`, `nom_user`, `date_naissance`, `email`, `mot_de_passe`, `telephone`, `fk_genre`) VALUES
(4, 'cgnydg', '', '0000-00-00', '', '', 0, 0),
(5, 'dsbsad', '', '0000-00-00', '', '', 0, 0),
(6, 'aaef', '', '0000-00-00', '', '', 0, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  ADD PRIMARY KEY (`id_adresse`);

--
-- Index pour la table `t_article`
--
ALTER TABLE `t_article`
  ADD PRIMARY KEY (`id_article`);

--
-- Index pour la table `t_commander`
--
ALTER TABLE `t_commander`
  ADD PRIMARY KEY (`id_commander`);

--
-- Index pour la table `t_habiter`
--
ALTER TABLE `t_habiter`
  ADD PRIMARY KEY (`id_habiter`),
  ADD KEY `fk_user` (`fk_user`),
  ADD KEY `fk_adresse` (`fk_adresse`);

--
-- Index pour la table `t_payer`
--
ALTER TABLE `t_payer`
  ADD PRIMARY KEY (`id_payer`);

--
-- Index pour la table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `fk_genre` (`fk_genre`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  MODIFY `id_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_article`
--
ALTER TABLE `t_article`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_commander`
--
ALTER TABLE `t_commander`
  MODIFY `id_commander` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_habiter`
--
ALTER TABLE `t_habiter`
  MODIFY `id_habiter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_payer`
--
ALTER TABLE `t_payer`
  MODIFY `id_payer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
