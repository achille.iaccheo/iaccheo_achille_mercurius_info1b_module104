"""
    Fichier : gestion_user_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouteruser(FlaskForm):
    """
        Dans le formulaire "user_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    prenom_user_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_user_wtf = StringField("Clavioter le user ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(prenom_user_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer user")


class FormWTFUpdateuser(FlaskForm):
    """
        Dans le formulaire "user_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    prenom_user_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_user_update_wtf = StringField("Clavioter le user ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(prenom_user_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Update user")


class FormWTFDeleteuser(FlaskForm):
    """
        Dans le formulaire "user_delete_wtf.html"

        prenom_user_delete_wtf : Champ qui reçoit la valeur du user, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "user".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_user".
    """
    prenom_user_delete_wtf = StringField("Effacer ce user")
    submit_btn_del = SubmitField("Effacer user")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
