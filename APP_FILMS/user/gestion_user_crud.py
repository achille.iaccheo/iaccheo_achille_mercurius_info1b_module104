"""
    Fichier : gestion_user_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les user.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.user.gestion_user_wtf_forms import FormWTFAjouteruser
from APP_FILMS.user.gestion_user_wtf_forms import FormWTFDeleteuser
from APP_FILMS.user.gestion_user_wtf_forms import FormWTFUpdateuser

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /user_afficher
    
    Test : ex : http://127.0.0.1:5005/user_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_user_sel = 0 >> tous les user.
                id_user_sel = "n" affiche le user dont l'id est "n"
"""


@obj_mon_application.route("/user_afficher/<string:order_by>/<int:id_user_sel>", methods=['GET', 'POST'])
def user_afficher(order_by, id_user_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion user ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionuser {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_user_sel == 0:
                    strsql_user_afficher = """SELECT id_user, prenom_user FROM t_user ORDER BY id_user ASC"""
                    mc_afficher.execute(strsql_user_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_user"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                    valeur_id_user_selected_dictionnaire = {"value_id_user_selected": id_user_sel}
                    strsql_user_afficher = """SELECT id_user, prenom_user FROM t_user WHERE id_user = %(value_id_user_selected)s"""

                    mc_afficher.execute(strsql_user_afficher, valeur_id_user_selected_dictionnaire)
                else:
                    strsql_user_afficher = """SELECT id_user, prenom_user FROM t_user ORDER BY id_user DESC"""

                    mc_afficher.execute(strsql_user_afficher)

                data_user = mc_afficher.fetchall()

                print("data_user ", data_user, " Type : ", type(data_user))

                # Différencier les messages si la table est vide.
                if not data_user and id_user_sel == 0:
                    flash("""La table "t_user" est vide. !!""", "warning")
                elif not data_user and id_user_sel > 0:
                    # Si l'utilisateur change l'id_user dans l'URL et que le user n'existe pas,
                    flash(f"Le user demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_user" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données user affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. user_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} user_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("user/user_afficher.html", data=data_user)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /user_ajouter
    
    Test : ex : http://127.0.0.1:5005/user_ajouter
    
    Paramètres : sans
    
    But : Ajouter un user pour un user
    
    Remarque :  Dans le champ "name_user_html" du formulaire "user/user_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/user_ajouter", methods=['GET', 'POST'])
def user_ajouter_wtf():
    form = FormWTFAjouteruser()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion user ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionuser {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_user_wtf = form.prenom_user_wtf.data

                name_user = name_user_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_prenom_user": name_user}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_user = """INSERT INTO t_user (id_user,prenom_user) VALUES (NULL,%(value_prenom_user)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_user, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('user_afficher', order_by='DESC', id_user_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_user_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_user_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion user CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("user/user_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /user_update
    
    Test : ex cliquer sur le menu "user" puis cliquer sur le bouton "EDIT" d'un "user"
    
    Paramètres : sans
    
    But : Editer(update) un user qui a été sélectionné dans le formulaire "user_afficher.html"
    
    Remarque :  Dans le champ "nom_user_update_wtf" du formulaire "user/user_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/user_update", methods=['GET', 'POST'])
def user_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_user"
    id_user_update = request.values['id_user_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateuser()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "user_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_user_update = form_update.prenom_user_update_wtf.data
            name_user_update = name_user_update.lower()

            valeur_update_dictionnaire = {"value_id_user": id_user_update, "value_name_user": name_user_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleuser = """UPDATE t_user SET prenom_user = %(value_name_user)s WHERE id_user = %(value_id_user)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleuser, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_user_update"
            return redirect(url_for('user_afficher', order_by="ASC", id_user_sel=id_user_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_user" et "prenom_user" de la "t_user"
            str_sql_id_user = "SELECT id_user, prenom_user FROM t_user WHERE id_user = %(value_id_user)s"
            valeur_select_dictionnaire = {"value_id_user": id_user_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_user, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom user" pour l'UPDATE
            data_nom_user = mybd_curseur.fetchone()
            print("data_nom_user ", data_nom_user, " type ", type(data_nom_user), " user ",
                  data_nom_user["prenom_user"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "user_update_wtf.html"
            form_update.prenom_user_update_wtf.data = data_nom_user["prenom_user"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans user_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("user/user_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /user_delete
    
    Test : ex. cliquer sur le menu "user" puis cliquer sur le bouton "DELETE" d'un "user"
    
    Paramètres : sans
    
    But : Effacer(delete) un user qui a été sélectionné dans le formulaire "user_afficher.html"
    
    Remarque :  Dans le champ "nom_user_delete_wtf" du formulaire "user/user_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/user_delete", methods=['GET', 'POST'])
def user_delete_wtf():
    data_users_attribue_adresse_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_user"
    id_user_delete = request.values['id_user_btn_delete_html']

    # Objet formulaire pour effacer le user sélectionné.
    form_delete = FormWTFDeleteuser()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("user_afficher", order_by="ASC", id_user_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "user/user_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_users_attribue_auser_delete = session['data_users_attribue_adresse_delete']
                print("data_users_attribue_adresse_delete ", data_users_attribue_adresse_delete)

                flash(f"Effacer le user de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer user" qui va irrémédiablement EFFACER le user
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_user": id_user_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_habiter = """DELETE FROM t_habiter WHERE fk_user = %(value_id_user)s"""
                str_sql_delete_iduser = """DELETE FROM t_user WHERE id_user = %(value_id_user)s"""
                # Manière brutale d'effacer d'abord la "fk_user", même si elle n'existe pas dans la "t_habiter"
                # Ensuite on peut effacer le user vu qu'il n'est plus "lié" (INNODB) dans la "t_habiter"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_habiter, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_iduser, valeur_delete_dictionnaire)

                flash(f"user définitivement effacé !!", "success")
                print(f"user définitivement effacé !!")

                # afficher les données
                return redirect(url_for('user_afficher', order_by="ASC", id_user_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_user": id_user_delete}
            print(id_user_delete, type(id_user_delete))

            # Requête qui affiche tous les habiter qui ont le user que l'utilisateur veut effacer
            str_sql_habiters_delete = """SELECT id_habiter, id_user, prenom_user, id_adresse, ville FROM t_habiter 
                                            INNER JOIN t_user ON t_habiter.fk_user = t_user.id_user
                                            INNER JOIN t_adresse ON t_habiter.fk_adresse = t_adresse.id_adresse
                                            WHERE fk_user = %(value_id_user)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_habiters_delete, valeur_select_dictionnaire)
            data_users_attribue_adresse_delete = mybd_curseur.fetchall()
            print("data_users_attribue_adresse_delete...", data_users_attribue_adresse_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "user/user_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_users_attribue_adresse_delete'] = data_users_attribue_adresse_delete

            # Opération sur la BD pour récupérer "id_user" et "prenom_user" de la "t_user"
            str_sql_id_user = "SELECT id_user, prenom_user FROM t_user WHERE id_user = %(value_id_user)s"

            mybd_curseur.execute(str_sql_id_user, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom user" pour l'action DELETE
            data_nom_user = mybd_curseur.fetchone()
            print("data_nom_user ", data_nom_user, " type ", type(data_nom_user), " user ",
                  data_nom_user["prenom_user"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "user_delete_wtf.html"
            form_delete.prenom_user_delete_wtf.data = data_nom_user["prenom_user"]

            # Le bouton pour l'action "DELETE" dans le form. "user_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans user_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans user_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans user_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans user_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("user/user_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_users_associes=data_users_attribue_adresse_delete)
