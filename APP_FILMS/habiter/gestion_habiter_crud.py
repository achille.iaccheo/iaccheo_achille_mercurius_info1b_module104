"""
    Fichier : gestion_habiter_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les user et les adresse.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : habiter_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /habiter_afficher
    
    But : Afficher les user avec les adresse associés pour chaque user.
    
    Paramètres : id_adresse_sel = 0 >> tous les user.
                 id_adresse_sel = "n" affiche le user dont l'id est "n"
                 
"""


@obj_mon_application.route("/habiter_afficher/<int:id_user_sel>", methods=['GET', 'POST'])
def habiter_afficher(id_user_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_habiter_afficher:
                code, msg = Exception_init_habiter_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_habiter_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_habiter_afficher.args[0]} , "
                      f"{Exception_init_habiter_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_habiter_afficher_data = """SELECT id_user, prenom_user,
                                                            GROUP_CONCAT(ville) as adresseuser FROM t_habiter
                                                            RIGHT JOIN t_user ON t_user.id_user = t_habiter.fk_user
                                                            LEFT JOIN t_adresse ON t_adresse.id_adresse = t_habiter.fk_adresse
                                                            GROUP BY id_user"""
                if id_user_sel == 0:
                    # le paramètre 0 permet d'afficher tous les user
                    # Sinon le paramètre représente la valeur de l'id du user
                    mc_afficher.execute(strsql_habiter_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                    valeur_id_user_selected_dictionnaire = {"value_id_user_selected": id_user_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_habiter_afficher_data += """ HAVING id_user= %(value_id_user_selected)s"""

                    mc_afficher.execute(strsql_habiter_afficher_data, valeur_id_user_selected_dictionnaire)

                # Récupère les données de la requête.
                data_habiter_afficher = mc_afficher.fetchall()
                print("data_adresse ", data_habiter_afficher, " Type : ", type(data_habiter_afficher))

                # Différencier les messages.
                if not data_habiter_afficher and id_user_sel == 0:
                    flash("""La table "t_user" est vide. !""", "warning")
                elif not data_habiter_afficher and id_user_sel > 0:
                    # Si l'utilisateur change l'id_user dans l'URL et qu'il ne correspond à aucun user
                    flash(f"Le user {id_user_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données user et adresse affichés !!", "success")

        except Exception as Exception_habiter_afficher:
            code, msg = Exception_habiter_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception habiter_afficher : {sys.exc_info()[0]} "
                  f"{Exception_habiter_afficher.args[0]} , "
                  f"{Exception_habiter_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("habiter/habiter_afficher.html", data=data_habiter_afficher)


"""
    nom: edit_habiter_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les adresse du user sélectionné par le bouton "MODIFIER" de "habiter_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les adresse contenus dans la "t_adresse".
    2) Les adresse attribués au user selectionné.
    3) Les adresse non-attribués au user sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_habiter_selected", methods=['GET', 'POST'])
def edit_habiter_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_adresse_afficher = """SELECT id_adresse, ville FROM t_adresse ORDER BY id_adresse ASC"""
                mc_afficher.execute(strsql_adresse_afficher)
            data_adresse_all = mc_afficher.fetchall()
            print("dans edit_habiter_selected ---> data_adresse_all", data_adresse_all)

            # Récupère la valeur de "id_user" du formulaire html "habiter_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_user"
            # grâce à la variable "id_habiter_edit_html" dans le fichier "habiter_afficher.html"
            # href="{{ url_for('edit_habiter_selected', id_habiter_edit_html=row.id_user) }}"
            id_habiter_edit = request.values['id_habiter_edit_html']

            # Mémorise l'id du user dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_habiter_edit'] = id_habiter_edit

            # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
            valeur_id_user_selected_dictionnaire = {"value_id_user_selected": id_habiter_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction habiter_afficher_data
            # 1) Sélection du user choisi
            # 2) Sélection des adresse "déjà" attribués pour le user.
            # 3) Sélection des adresse "pas encore" attribués pour le user choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "habiter_afficher_data"
            data_habiter_selected, data_habiter_non_attribues, data_habiter_attribues = \
                habiter_afficher_data(valeur_id_user_selected_dictionnaire)

            print(data_habiter_selected)
            lst_data_user_selected = [item['id_user'] for item in data_habiter_selected]
            print("lst_data_user_selected  ", lst_data_user_selected,
                  type(lst_data_user_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresse qui ne sont pas encore sélectionnés.
            lst_data_habiter_non_attribues = [item['id_adresse'] for item in data_habiter_non_attribues]
            session['session_lst_data_habiter_non_attribues'] = lst_data_habiter_non_attribues
            print("lst_data_habiter_non_attribues  ", lst_data_habiter_non_attribues,
                  type(lst_data_habiter_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresse qui sont déjà sélectionnés.
            lst_data_habiter_old_attribues = [item['id_adresse'] for item in data_habiter_attribues]
            session['session_lst_data_habiter_old_attribues'] = lst_data_habiter_old_attribues
            print("lst_data_habiter_old_attribues  ", lst_data_habiter_old_attribues,
                  type(lst_data_habiter_old_attribues))

            print(" data data_habiter_selected", data_habiter_selected, "type ", type(data_habiter_selected))
            print(" data data_habiter_non_attribues ", data_habiter_non_attribues, "type ",
                  type(data_habiter_non_attribues))
            print(" data_habiter_attribues ", data_habiter_attribues, "type ",
                  type(data_habiter_attribues))

            # Extrait les valeurs contenues dans la table "t_adresse", colonne "ville"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_adresse
            lst_data_habiter_non_attribues = [item['ville'] for item in data_habiter_non_attribues]
            print("lst_all_adresse gf_edit_habiter_selected ", lst_data_habiter_non_attribues,
                  type(lst_data_habiter_non_attribues))

        except Exception as Exception_edit_habiter_selected:
            code, msg = Exception_edit_habiter_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_habiter_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_habiter_selected.args[0]} , "
                  f"{Exception_edit_habiter_selected}", "danger")

    return render_template("habiter/habiter_modifier_tags_dropbox.html",
                           data_adresse=data_adresse_all,
                           data_user_selected=data_habiter_selected,
                           data_adresse_attribues=data_habiter_attribues,
                           data_adresse_non_attribues=data_habiter_non_attribues)


"""
    nom: update_habiter_selected

    Récupère la liste de tous les adresse du user sélectionné par le bouton "MODIFIER" de "habiter_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les adresse contenus dans la "t_adresse".
    2) Les adresse attribués au user selectionné.
    3) Les adresse non-attribués au user sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_habiter_selected", methods=['GET', 'POST'])
def update_habiter_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du user sélectionné
            id_user_selected = session['session_id_habiter_edit']
            print("session['session_id_habiter_edit'] ", session['session_id_habiter_edit'])

            # Récupère la liste des adresse qui ne sont pas associés au user sélectionné.
            old_lst_data_habiter_non_attribues = session['session_lst_data_habiter_non_attribues']
            print("old_lst_data_habiter_non_attribues ", old_lst_data_habiter_non_attribues)

            # Récupère la liste des adresse qui sont associés au user sélectionné.
            old_lst_data_habiter_attribues = session['session_lst_data_habiter_old_attribues']
            print("old_lst_data_habiter_old_attribues ", old_lst_data_habiter_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme adresse dans le composant "tags-selector-tagselect"
            # dans le fichier "habiter_modifier_tags_dropbox.html"
            new_lst_str_habiter = request.form.getlist('name_select_tags')
            print("new_lst_str_habiter ", new_lst_str_habiter)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_habiter_old = list(map(int, new_lst_str_habiter))
            print("new_lst_habiter ", new_lst_int_habiter_old, "type new_lst_habiter ",
                  type(new_lst_int_habiter_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_adresse" qui doivent être effacés de la table intermédiaire "t_habiter".
            lst_diff_adresse_delete_b = list(
                set(old_lst_data_habiter_attribues) - set(new_lst_int_habiter_old))
            print("lst_diff_adresse_delete_b ", lst_diff_adresse_delete_b)

            # Une liste de "id_adresse" qui doivent être ajoutés à la "t_habiter"
            lst_diff_adresse_insert_a = list(
                set(new_lst_int_habiter_old) - set(old_lst_data_habiter_attribues))
            print("lst_diff_adresse_insert_a ", lst_diff_adresse_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_user"/"id_user" et "fk_adresse"/"id_adresse" dans la "t_habiter"
            strsql_insert_habiter = """INSERT INTO t_habiter (id_habiter, fk_adresse, fk_user)
                                                    VALUES (NULL, %(value_fk_adresse)s, %(value_fk_user)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_user" et "id_adresse" dans la "t_habiter"
            strsql_delete_habiter = """DELETE FROM t_habiter WHERE fk_adresse = %(value_fk_adresse)s AND fk_user = %(value_fk_user)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le user sélectionné, parcourir la liste des adresse à INSÉRER dans la "t_habiter".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_adresse_ins in lst_diff_adresse_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                    # et "id_adresse_ins" (l'id du adresse dans la liste) associé à une variable.
                    valeurs_user_sel_adresse_sel_dictionnaire = {"value_fk_user": id_user_selected,
                                                               "value_fk_adresse": id_adresse_ins}

                    mconn_bd.mabd_execute(strsql_insert_habiter, valeurs_user_sel_adresse_sel_dictionnaire)

                # Pour le user sélectionné, parcourir la liste des adresse à EFFACER dans la "t_habiter".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_adresse_del in lst_diff_adresse_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                    # et "id_adresse_del" (l'id du adresse dans la liste) associé à une variable.
                    valeurs_user_sel_adresse_sel_dictionnaire = {"value_fk_user": id_user_selected,
                                                               "value_fk_adresse": id_adresse_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_habiter, valeurs_user_sel_adresse_sel_dictionnaire)

        except Exception as Exception_update_habiter_selected:
            code, msg = Exception_update_habiter_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_habiter_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_habiter_selected.args[0]} , "
                  f"{Exception_update_habiter_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_habiter",
    # on affiche les user et le(urs) adresse(s) associé(s).
    return redirect(url_for('habiter_afficher', id_user_sel=id_user_selected))


"""
    nom: habiter_afficher_data

    Récupère la liste de tous les adresse du user sélectionné par le bouton "MODIFIER" de "habiter_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des adresse, ainsi l'utilisateur voit les adresse à disposition

    On signale les erreurs importantes
"""


def habiter_afficher_data(valeur_id_user_selected_dict):
    print("valeur_id_user_selected_dict...", valeur_id_user_selected_dict)
    try:

        strsql_user_selected = """SELECT id_user, prenom_user, GROUP_CONCAT(id_adresse) as adresseuser FROM t_habiter
                                        INNER JOIN t_user ON t_user.id_user = t_habiter.fk_user
                                        INNER JOIN t_adresse ON t_adresse.id_adresse = t_habiter.fk_adresse
                                        WHERE id_user = %(value_id_user_selected)s"""

        strsql_habiter_non_attribues = """SELECT id_adresse, ville FROM t_adresse WHERE id_adresse not in(SELECT id_adresse as idadresseuser FROM t_habiter
                                                    INNER JOIN t_user ON t_user.id_user = t_habiter.fk_user
                                                    INNER JOIN t_adresse ON t_adresse.id_adresse = t_habiter.fk_adresse
                                                    WHERE id_user = %(value_id_user_selected)s)"""

        strsql_habiter_attribues = """SELECT id_user, id_adresse, ville FROM t_habiter
                                            INNER JOIN t_user ON t_user.id_user = t_habiter.fk_user
                                            INNER JOIN t_adresse ON t_adresse.id_adresse = t_habiter.fk_adresse
                                            WHERE id_user = %(value_id_user_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_habiter_non_attribues, valeur_id_user_selected_dict)
            # Récupère les données de la requête.
            data_habiter_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("habiter_afficher_data ----> data_habiter_non_attribues ", data_habiter_non_attribues,
                  " Type : ",
                  type(data_habiter_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_user_selected, valeur_id_user_selected_dict)
            # Récupère les données de la requête.
            data_user_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_user_selected  ", data_user_selected, " Type : ", type(data_user_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_habiter_attribues, valeur_id_user_selected_dict)
            # Récupère les données de la requête.
            data_habiter_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_habiter_attribues ", data_habiter_attribues, " Type : ",
                  type(data_habiter_attribues))

            # Retourne les données des "SELECT"
            return data_user_selected, data_habiter_non_attribues, data_habiter_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans habiter_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans habiter_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_habiter_afficher_data:
        code, msg = IntegrityError_habiter_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans habiter_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_habiter_afficher_data.args[0]} , "
              f"{IntegrityError_habiter_afficher_data}", "danger")
